# wakeUPlight



## Introduction

This is the wakeUPlight. UP can be taken literally. An addressable LED strip, mounted vertically, simulates a sunrise!

## Status
I've created this thingy a couple of years ago. At the moment I'm re-writing the code from scratch, as the old code got very messy. 

## Some ideas
In the end, some thoughts of where I'd like to go:
- A web interface / app to configure the wakeUPlight
	- Nice to have: automatically sync to alarm(s) set on smartphone
- A physical user interface. Can we make it capacitive? 
- Use an optical time of flight sensor to measure location of hand, for a user interface
- Probably more. I'm not quickly satisfied. 

