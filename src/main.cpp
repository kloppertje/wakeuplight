// Initial example based on https://werner.rothschopf.net/202011_arduino_esp8266_ntp_en.htm

#include <Arduino.h>
#include <ESP8266WiFi.h>            // we need wifi to get internet access
#include <time.h>                   // time() ctime()
#include <coredecls.h>  // settimeofday_cb()
#include <FastLED.h>
#include <Streaming.h>

#if __has_include("wifiCredentials.h")
  #include "wifiCredentials.h"
#else
  #define WIFI_SSID "your_wifi_ssid"
  #define WIFI_PSK  "your_wifi_password"
#endif


// Configuration of NTP
#define NTP_SERVER "pool.ntp.org"           
#define NTP_TIMEZONE "CET-1CEST,M3.5.0,M10.5.0/3" // NL, Amsterdam time zone

// Configuration of addressable LEDs
#define PIN_LED_DATA D8
#define LED_COLOR_ORDER   GRB
const uint16_t NUM_LEDS = 59;
CRGB leds[NUM_LEDS];

// Configuration of alarm
struct {
  union {
    struct {
      uint8_t enable;
      uint8_t hour;
      uint8_t minute;
    };
    uint8_t values[3];
  };
} alarm = {0, 6, 45};

struct {
  uint8_t enable = 0;
  uint8_t brightness = 255;
  uint8_t durationStep1Sec = 10*60;
  uint8_t durationStep2Sec = 10*60;
  unsigned long tEnable;
  uint8_t hueMin = 255;
  uint8_t hueMax = 130;
  uint8_t saturation = 255;
} wul;



void showTime(time_t now) {
  // time(&now);                       // read the current time
  tm tm;
  localtime_r(&now, &tm);           // update the structure tm with the current time
  Serial.print("now:");
  Serial.print(now);
  Serial.print("\tyear:");
  Serial.print(tm.tm_year + 1900);  // years since 1900
  Serial.print("\tmonth:");
  Serial.print(tm.tm_mon + 1);      // January = 0 (!)
  Serial.print("\tday:");
  Serial.print(tm.tm_mday);         // day of month
  Serial.print("\thour:");
  Serial.print(tm.tm_hour);         // hours since midnight  0-23
  Serial.print("\tmin:");
  Serial.print(tm.tm_min);          // minutes after the hour  0-59
  Serial.print("\tsec:");
  Serial.print(tm.tm_sec);          // seconds after the minute  0-61*
  Serial.print("\twday:");
  Serial.print(tm.tm_wday);         // days since Sunday 0-6
  if (tm.tm_isdst == 1)             // Daylight Saving Time flag
    Serial.print("\tDST");
  else
    Serial.print("\tstandard");
  Serial.println();
}

void time_is_set(bool from_sntp) {
 Serial.print("Time was set by ");
  if (from_sntp) {
    Serial.println("SNTP");
  } else {
    Serial.println("USER");
  }
}

void setup() {
  Serial.begin(115200);
  Serial.println("wakeUPlight is getting started...");

  // Init LEDs
  FastLED.addLeds<WS2812B, PIN_LED_DATA, LED_COLOR_ORDER>(leds, NUM_LEDS);
  leds[0] = CHSV(200, 255, 150); // Show that we're alive
  FastLED.show();


  // install callback - called when settimeofday is called (by SNTP or user)
  // once enabled (by DHCP), SNTP is updated every hour by default
  // ** optional boolean in callback function is true when triggered by SNTP **
  settimeofday_cb(time_is_set);
  configTime(NTP_TIMEZONE, NTP_SERVER);

  // start network
  Serial.print("Connecting to WiFi network ");
  Serial.println(WIFI_SSID);
  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_SSID, WIFI_PSK);
  uint8_t hue = 0; // Slowly change colour of LED while connecting
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print ( "." );
    hue += 10;
    leds[1] = CHSV(hue, 255, 150);
    FastLED.show();
    delay(100);
  }
  Serial.println("\nWiFi connected");

  FastLED.clear();
  FastLED.show();
  FastLED.show();
  
}

void loop() {
  static time_t lastNow = 0;
  time_t now;
  tm tm;

  time(&now); // Read the current time
  if (now!=lastNow) {
    lastNow = now;
    showTime(now);
    localtime_r(&now, &tm);           // update the structure tm with the current time
    
    // Check if alarm matches current time
    if (alarm.enable) {
      if (alarm.hour==tm.tm_hour && alarm.minute==tm.tm_min && tm.tm_sec==0) {
        Serial.println("Boo");
        wul.enable = 1;
      }
    }
  }

  // WUL action
  static unsigned long tWulLast = 0;
  static unsigned long tWulEnable = 0;
  unsigned long wulDurationStep1Ms = wul.durationStep1Sec * 1000;
  unsigned long tNow = millis();
  uint32_t wulNoFrames = NUM_LEDS * 255;
  uint16_t wulDurationStep1PerLedMs = wulDurationStep1Ms / NUM_LEDS;
  uint16_t wulDurationStep1PerFrameMs = wulDurationStep1Ms / wulNoFrames;

  uint16_t wulDurationStep2PerFrameMs = wul.durationStep2Sec * 1000 / 255;
  static uint8_t lastWulEnable = 0;

  if (wul.enable) {
    if (!lastWulEnable && wul.enable) {
      FastLED.clear();
      tWulEnable = tNow;
    }
    if (wul.enable==1) {
      if (tNow - tWulLast > wulDurationStep1PerFrameMs) {
        tWulLast = tNow;
        uint16_t ledPos = ((tNow - tWulEnable) / wulDurationStep1PerLedMs);
        if (ledPos == NUM_LEDS) {
          wul.enable = 2; // If all LEDs are lit, we are done here. Move on to step 2
          tWulEnable = tNow; // Set starting time for step 2
          tWulLast = 0; // Reset, such that step 2 will immediately give an update
        }

        uint8_t hue = wul.hueMin + (ledPos * (wul.hueMax - wul.hueMin)) / NUM_LEDS;
        uint16_t ledTime = (tNow - tWulEnable) % wulDurationStep1PerLedMs;
        uint16_t value = (ledTime * 255) / wulDurationStep1PerLedMs;

        Serial << ledPos << "\t" << hue << "\t" << ledTime << "\t" << value << endl;

        leds[ledPos] = CHSV(hue, wul.saturation, value);
        FastLED.show();
      }
    } else if (wul.enable==2) { // In step 2, slowly decrease saturation, such that light goes to maximum brightness
      if (tNow-tWulLast > wulDurationStep2PerFrameMs) {
        tWulLast = tNow;
        int16_t sat = 255 - (tNow-tWulEnable)/wulDurationStep2PerFrameMs;
        
        if (sat>0) {
          for (uint8_t k=0; k<NUM_LEDS; k++) {
            uint8_t hue = wul.hueMin + (k * (wul.hueMax - wul.hueMin)) / NUM_LEDS;
            leds[k] = CHSV(hue, sat, 255);
          }
          FastLED.show();
        } else {
          wul.enable = 0; // Done!
        }
        Serial.println(sat);
      } 
      
    }
  }
  // if (!wul.enable && lastWulEnable) { // Someone turned off the WUL function
  //   FastLED.clear();
  //   FastLED.show();
  // }
  lastWulEnable = wul.enable;

  // Simple interface for sending commands via serial interface, useful for debugging
  if (Serial.available()>1) {
    char cmd = Serial.read();
    char val = Serial.parseInt();
    switch (cmd) {
      case 'm':
        alarm.minute = val;
        break;
      case 'h':
        alarm.hour = val;
        break;
      case 'e':
        alarm.enable = val;
        break;
      case 'w':
        wul.enable = val;
        break;
      case 'd':
        wul.durationStep1Sec = val;
        wul.durationStep2Sec = val;
        break;
    }
    Serial << "Alarm set to: " << alarm.hour << ":" << alarm.minute << ", enable: " << alarm.enable << endl;
    while (Serial.available()) Serial.read(); // Empty buffer
  }
  
  
}  